Voir la [[!RFC 1459]] pour le protocole IRC.

	<prefix>  ::= <servername> | <nick> [ ‘!’ <fingerprint>] [ ‘@’ <host>]
	<channel> ::= (‘#’ | ‘&’ | ‘%’) <chstring>

Notes:
``<servername>`` est utilisé pour rester compatible, les données du monde/de l'univers sont envoyé par un autre protocole.
Le pourcent(``%``) sert pour rejoindre un monde/une dimension.
