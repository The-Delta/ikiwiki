Cette page contient les informations concernant l’administration système. D’autres sujets seront peut-être a venir.

# Serveur proposé par [lanodan](membre/lanodan)

## système/matériel
Le serveur temporaire est un Olimex OLinuXino A20-LIME2-4G, [debian stable](http://www.igorpecovnik.com/2014/11/18/olimex-lime-debian-sd-image/)(non-officiel mais léger) est installé sur la NAND et fait office de système de secours, gentoo est mis sur la ųSD comme système d’exploitation principal.

Le stable serat un Banana Pi (sans doute du gentoo aussi).

## web
Le daemon est un [nginx](//nginx.org) avec fastcgi pour les ``.cgi``. Le wiki est propulsé par [ikiwiki](//ikiwiki.info), qui propose nativement git(ou autre CVS), d’être semi-statique(pages générée, le cgi est optionnel et sert pour l’édition)

## Contrôle des Versions
Géré par du git, hébergé sur gitlab [ici](//gitlab.com/The-Delta/ikiwiki). Un [irker](http://www.catb.org/esr/irker/) est configuré sur le serveur web. (Celui de gitlab n’ayant pas l’air de fonctionner)
